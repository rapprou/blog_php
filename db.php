<?php

    // Ne pas afficher les erreurs du serveur
    ini_set("display_errors", "off");

    // Initialiser une connexion à une base de données
    $conn = mysqli_connect("localhost", "admin", "fQF67UhwPVcN", "blog_wp");

    // créer une connexion
    if(!$conn){
        echo "<h3 class='container bg-dark p-3 text-center text-warning rounded-lg mt-5'>Not able to establish Database Connection<h3>";
    }

    // Obtenir des données à afficher sur la page index
    $sql = "SELECT * FROM data";
    $query = mysqli_query($conn, $sql);

    // Créer un nouveau message
    if(isset($_REQUEST['new_post'])){
        $title = $_REQUEST['title'];
        $content = $_REQUEST['content'];

        $sql = "INSERT INTO data(title, content) VALUES('$title', '$content')";
        mysqli_query($conn, $sql);

        echo $sql;

        header("Location: index.php?info=added");
        exit();
    }

    // Obtenir les données de publication en fonction de l'id.
    if(isset($_REQUEST['id'])){
        $id = $_REQUEST['id'];
        $sql = "SELECT * FROM blog_wp WHERE id = $id";
        $query = mysqli_query($conn, $sql);
    }

    // Supprimer un message
    if(isset($_REQUEST['delete'])){
        $id = $_REQUEST['id'];

        $sql = "DELETE FROM blog_wp WHERE id = $id";
        mysqli_query($conn, $sql);

        header("Location: index.php");
        exit();
    }

    // Mettre à jour un message
    if(isset($_REQUEST['update'])){
        $id = $_REQUEST['id'];
        $title = $_REQUEST['title'];
        $content = $_REQUEST['content'];

        $sql = "UPDATE blog_wp SET title = '$title', content = '$content' WHERE id = $id";
        mysqli_query($conn, $sql);

        header("Location: index.php");
        exit();
    }

?>